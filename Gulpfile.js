var gulp = require('gulp');

gulp.task('scripts', function() {
    gulp.src(['app/src/**/*.js'])
        .pipe(browserify())
        .pipe(concat('dest.js'))
        .pipe(gulp.dest('dist/build'))
        .pipe(refresh(server))
})

gulp.task('styles', function() {
    gulp.src(['app/css/style.less'])
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(gulp.dest('dist/build'))
        .pipe(refresh(server))
})

gulp.task('lr-server', function() {
    server.listen(35729, function(err) {
        if(err) return console.log(err);
    });
})

gulp.task('html', function() {
    gulp.src("app/*.html")
        .pipe(embedlr())
        .pipe(gulp.dest('dist/'))
        .pipe(refresh(server));
})

gulp.task('default', function() {
    //gulp.series('lr-server', 'scripts', 'styles', 'html');

    gulp.watch('app/src/**', function(event) {
        gulp.series('scripts');
    })

    gulp.watch('app/css/**', function(event) {
        gulp.series('styles');
        console.log ('compiling sass');
    })

    gulp.watch('app/**/*.html', function(event) {
        gulp.series('html');
    })
})